#!/usr/bin/env bash

    if [ "$1" == "up" ]; then
    	cp .env.example .env

       	docker-compose build

        docker-compose up -d

        docker-compose exec app composer install

        docker-compose exec app php artisan route:cache

		echo "Start queue worker"
        docker-compose exec -d app php artisan queue:work


    elif [ "$1" == "down" ]; then
    	docker-compose down
    fi