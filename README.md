## Usage

Create and start containers. Lunch application

```/deploy.sh up```

Stop and remove containers

```./deploy.sh down```


## Endpoints

Update statistics

```curl -X POST "http://localhost/api/stat/country" --data "country_code=it"```

Get statistics

```curl -X GET "http://localhost/api/stat/country"``` 