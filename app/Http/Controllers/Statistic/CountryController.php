<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/11/20
 * Time: 7:31 PM
 */

namespace App\Http\Controllers\Statistic;

use App\Domain\Factory\FactoryServiceCountryStatistic;
use App\Http\Controllers\Controller;
use App\Jobs\CountryStatisticViewIncrement;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Infrastructure\Enum\Error as EnumError;


class CountryController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = FactoryServiceCountryStatistic::build();
    }

    /**
     * Update country statistics
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'country_code' => 'required|max:2|min:2'
        ]);

        if ($validator->fails()) {
            return $this->error(
                EnumError::ERROR_VALIDATION,
                $validator->errors()->all()
            );
        }

        try {
            //add job to queue
            CountryStatisticViewIncrement::dispatch($request->get('country_code'));
        } catch (Exception $ex) {
            error_log($ex);
            return $this->error(
                EnumError::ERROR_503,
                ['Service temporarily unavailable']
            );
        }

        return $this->success(['success' => true]);
    }

    /**
     * Get country statistics
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllStatistic(Request $request)
    {
        try {
            $data = $this->service->getAll();
            return $this->success($data);
        } catch (Exception $ex) {
            error_log($ex);
            return $this->error(
                EnumError::ERROR_503,
                ['Service temporarily unavailable']
            );
        }
    }
}