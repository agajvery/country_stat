<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/11/20
 * Time: 10:32 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request)
    {
        return view('welcome');
    }
}