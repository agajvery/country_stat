<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 9:58 AM
 */

namespace App\Providers;


use Illuminate\Support\Facades\Redis;
use App\Domain\Provider\RedisInterface;

class LaravelRedis implements RedisInterface
{
    /**
     * @var \Illuminate\Redis\Connections\Connection
     */
    private $redis;

    public function __construct($connectionName = 'default')
    {
        $this->redis = Redis::connection($connectionName);
    }

    public function sadd($key, $member): bool
    {
        return (bool) $this->redis->sadd($key, $member);
    }

    public function incr(string $key): int
    {
        return $this->redis->incr($key);
    }

    public function smembers(string $key): array
    {
        $data = $this->redis->smembers($key);
        return is_array($data) ? $data : [];
    }

    public function get(string $key): ?string
    {
        return $this->redis->get($key);
    }
}