<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 8:12 AM
 */

namespace App\Domain\Repository;

use App\Domain\Entity\CountryStat;
use App\Domain\Provider\RedisInterface;

class RedisCountryStatistic implements CountryStatisticInterface
{
    const COUNTRY_LIST_KEY = 'countries';

    private $redis;

    public function __construct(RedisInterface $redis)
    {
        $this->redis = $redis;
    }

    public function increment(CountryStat $entity): int
    {
        $this->redis->sadd(self::COUNTRY_LIST_KEY, $entity->getCountryCode());
        return $this->redis->incr($this->getKey($entity->getCountryCode()));
    }

    public function getAll(): array
    {
        $statistic = [];

        $countries = $this->redis->smembers(self::COUNTRY_LIST_KEY);
        foreach ($countries as $countryCode) {
            $statistic[$countryCode] = $this->redis->get($this->getKey($countryCode));
        }

        return $statistic;
    }

    private function getKey(string $countryCode)
    {
        return 'number_view_' . $countryCode;
    }
}