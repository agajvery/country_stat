<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 7:57 AM
 */

namespace App\Domain\Repository;

use App\Domain\Entity\CountryStat;

interface CountryStatisticInterface
{
    public function increment(CountryStat $entity): int;

    public function getAll(): array;
}