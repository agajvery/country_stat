<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 11:23 AM
 */

namespace App\Domain\Factory;


use App\Domain\Service\CountryStatisticInterface;
use App\Providers\LaravelRedis;
use App\Domain\Service\CountryStatistic as ServiceCountryStatistic;
use App\Domain\Repository\RedisCountryStatistic as RepositoryRedisCountryStatistic;


class FactoryServiceCountryStatistic
{
    public static function build(): CountryStatisticInterface
    {
        $redis = new LaravelRedis();

        $repository = new RepositoryRedisCountryStatistic($redis);
        return new ServiceCountryStatistic($repository);
    }
}