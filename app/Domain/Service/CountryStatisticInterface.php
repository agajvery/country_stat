<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 11:25 AM
 */

namespace App\Domain\Service;


interface CountryStatisticInterface
{
    public function incrementView(string $countryCode);

    public function getAll(): array;
}