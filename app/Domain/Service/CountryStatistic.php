<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 7:51 AM
 */

namespace App\Domain\Service;

use App\Domain\Entity\CountryStat;
use App\Domain\Repository\CountryStatisticInterface as RepositoryCountryStatisticInterface;

class CountryStatistic implements CountryStatisticInterface
{
    /**
     * @var RepositoryCountryStatisticInterface
     */
    private $repository;

    /**
     * CountryStatistic constructor.
     * @param RepositoryCountryStatisticInterface $repository
     */
    public function __construct(RepositoryCountryStatisticInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * increment number of views
     *
     * @param string $countryCode
     */
    public function incrementView(string $countryCode)
    {
        $entity = new CountryStat($countryCode);
        $this->repository->increment($entity);
    }

    /**
     * get number of views for all countries
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->repository->getAll();
    }
}