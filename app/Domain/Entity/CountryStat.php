<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 7:46 AM
 */

namespace App\Domain\Entity;


class CountryStat
{
    private $countryCode;

    private $numberViews = 0;

    public function __construct(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getNumberViews(): int
    {
        return $this->numberViews;
    }

    public function setNumberViews(int $value)
    {
        $this->numberViews = $value;
    }
}