<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 9:00 AM
 */

namespace App\Domain\Provider;


interface RedisInterface
{
    public function sadd(string $key, string $member): bool;

    public function incr(string $key): int;

    public function smembers(string $key): array;

    public function get(string $key): ?string;
}