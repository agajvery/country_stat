<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 9:12 AM
 */

namespace App\Domain\Provider;


interface QueueInterface
{
    public function publish(array $channels, string $jsonData);
    public function subscribe(array $channels, callable $handler);
}